import fastify from 'fastify'

import helmet from 'fastify-helmet'
import fastifyJWT from 'fastify-jwt'
import bcrypt from 'fastify-bcrypt'


import { APP_PORT } from './configs/constant'
import connectMongo from './configs/mongo'
import jwtOptions from './configs/jwt'
import { fastifyOptions } from './configs/fastify'
import { registerRoutes } from './routes'
import { decorators } from './decorators'


const start = (callback) => {
    const app = fastify(fastifyOptions)

    connectMongo()

    app.register(fastifyJWT, jwtOptions)
    app.register(helmet)
    app.register(require('fastify-cors'), { origin: '*' });
    app.register(bcrypt, {
        saltWorkFactor: 2
    })

    callback(app)

    app.listen(APP_PORT, (err) => {
        if (err) app.log.error(err), process.exit(1)
    })
}


export default start((app) => {
    decorators(app)
    registerRoutes(app)
})