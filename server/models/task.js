import mongoose from 'mongoose'

const { Schema } = mongoose
const { Types } = Schema

const TaskSchema = new Schema({
    name: { type: Types.String, required: true, default: 'Untitled Task' },
    isDeleted: { type: Types.Boolean, required: true, default: false },
    members: [{ type: Types.ObjectId, ref: 'user' }],
})

export default mongoose.model('task', TaskSchema)