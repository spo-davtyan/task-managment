import mongoose from 'mongoose'

const { Schema } = mongoose
const { Types } = Schema

const ColumnSchema = new Schema({
    name: { type: Types.String, required: true, default: 'Untitled Column' },
    isDeleted: { type: Types.Boolean, required: true, default: false },
    tasks: [{ type: Types.ObjectId, ref: 'task' }]
})

export default mongoose.model('column', ColumnSchema)