import mongoose from 'mongoose'

const { Schema } = mongoose
const { Types } = Schema

const UserSchema = new Schema({
    name: { type: Types.String, required: true },
    email: { type: Types.String, unique: true, required: true, dropDups: true },
    password: { type: Types.String, required: true },
    projects: [{ type: Types.ObjectId, ref: 'project' }],
})

export default mongoose.model('users', UserSchema)