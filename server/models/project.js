import mongoose from 'mongoose'

const { Schema } = mongoose
const { Types } = Schema

const ProjectSchema = new Schema({
    name: { type: Types.String, required: true, default: 'Untitled Project' },
    isDeleted: { type: Types.Boolean, required: true, default: false },
    columns: [{ type: Types.ObjectId, ref: 'column' }],
    users: Types.Mixed,
})

export default mongoose.model('project', ProjectSchema)