import mongoose from 'mongoose'
const mongoUrl = process.env.MONGODB_URI || "mongodb://localhost:27017/fast"
export default () => {
    try {
        return mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
    } catch (error) {
        console.error(error)
    }
}