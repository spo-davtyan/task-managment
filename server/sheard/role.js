import { ROLE } from "./../configs/constant";
export const addRoleToUser = () => {

}

export const getUserIdRoleId = (id, roleId) => {
    return {
        id,
        roleId
    }
}

export const GetObject = (obj) => {
    console.log(obj.users);
    const getAccess = (userId) => obj.users[userId]
    const checkAccess = (userId) => !!getAccess(userId)
    const checkUserAccess = (userId) => checkAccess(userId) && getAccess(userId) === ROLE.OWNER
    const checkOwnerAccess = (userId) => checkAccess(userId) && getAccess(userId) === ROLE.OWNER

    return {
        getAccess,
        checkAccess,
        checkUserAccess,
        checkOwnerAccess,
    }
} 