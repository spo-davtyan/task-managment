import Joi from "joi";

export const body = (props) => {
    return {
        schema: {
            body: Joi.object().keys(props).required()
        },
        validatorCompiler: (options) => data => {
            return options.schema.validate(data)
        },
    }
}