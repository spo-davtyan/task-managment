const generateToken = async (reply, payload, expiresIn = '1 day' /* second */) => await reply.jwtSign(payload, { expiresIn })

export const getTokens = async (reply, id) => {
    console.log(id);
    const access = await generateToken(reply, { id }, '1 day')
    const now = new Date();
    const refresh = await generateToken(
        reply,
        {
            id,
            loggedin: now
        },
        '30 day')
    return {
        access,
        refresh
    }
}