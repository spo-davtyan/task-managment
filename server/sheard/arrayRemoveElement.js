export function removeElement(array, item) {
     array.splice(array.indexOf(item), 1);
     return array;
}