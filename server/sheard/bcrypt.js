import bcrypt from 'bcrypt'
const salt = 2;
export const hashPassword = async (password) => await bcrypt.hash(password, salt);
export const checkPassword = async (password, hash) => await bcrypt.compare(password, hash);