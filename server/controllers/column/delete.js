import Column from "../../models/column";
import Project from "../../models/project";
import { removeElement } from "../../sheard/arrayRemoveElement";
import Task from "../../models/task";

async function handler(req, reply) {
    const { projectId, columnId } = req.params
    try {
        const column = await Column.findById(columnId);
        console.log(column)
        await Task.deleteMany({ _id: { $in: column.tasks } });
        let project = await Project.findById(projectId);
        project.columns = removeElement(project.columns, columnId)
        project = await project.save()

        reply.status(200).send()
    }
    catch (e) {
        console.log(e)
        reply.status(400).send()
    }


}
const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

