import { mapColumn } from "../../mappers/project";
import Column from "../../models/column";
import Project from "../../models/project";

async function handler(req, reply) {
    const { name, projectId, order } = req.body

    Column.create({
        name,
        order,
        tasks: []
    }).then((column) => {
        Project.findById(projectId).then(async (project) => {
            project.columns = [
                ...project.columns,
                column._id
            ]
            await project.save()
            reply.send(mapColumn(column))
        }).catch((error) => reply.status(400).send({ error: error.toString() }))
    }).catch((error) => reply.status(400).send({ error: error.toString() }))

}

const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

