import Joi from "joi";
import { mapUserWithToken } from "../../mappers/user";
import User from "../../models/user";
import { getTokens } from "../../sheard/jwt";
import { body } from "../../sheard/schema";

async function handler(req, reply) {
    const { email, password } = req.body
    User.findOne({ email })
        .then(async (user) => {
            if (user) {
                const checkPassword = await req.bcryptCompare(password, user.password)

                if (checkPassword) {
                    const id = user._id;
                    const token = await getTokens(reply, id);
                    return reply.send(mapUserWithToken({ user, token }));
                }

            }
            reply
                .code(404)
                .send({
                    error: `You are fill wrong cridentals`
                });
        })
        .catch(error => {
            console.log(5);
            reply.send({ error: error.toString() })
        })


}

const options = body({
    email: Joi.string().email({ minDomainSegments: 2 }).required(),
    password: Joi.string().min(3).max(15).required(),
});

export default {
    handler,
    options
}