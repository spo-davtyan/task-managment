import Joi from "joi";
import { body } from "../../sheard/schema";
import { getTokens } from "../../sheard/jwt";

async function handler(req, reply, app) {
    const { refresh } = req.body

    try {
        const { id } = app.jwt.verify(refresh)
        const token = await getTokens(reply, id);
        reply.send({ ...token })
    } catch (error) {
        reply.status(401).send({ error: error.toString() })
    }


}

const options = body({
    refresh: Joi.string().required(),
});

export default {
    handler,
    options
}

