import Joi from "joi";
import { body } from "../../sheard/schema";

async function handler(req, reply) {
    reply.send(req.user)
}

const options = (preValidation) => {
    return {
        ...body({
            token: Joi.string().required(),
        }),
        preValidation: [preValidation]
    }
};

export default {
    handler,
    options
}

