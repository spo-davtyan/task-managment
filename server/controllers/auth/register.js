import Joi from "joi";
import { mapUserWithToken, mapUser } from "../../mappers/user";
import User from "../../models/user";
import { getTokens } from "../../sheard/jwt";
import { body } from "../../sheard/schema";

async function handler(req, reply) {
    const { name, email, password } = req.body
    const hash = await req.bcryptHash(password)
    User.create({ name, email, password: hash })
        .then(async (user) => {
            const { id } = mapUser(user);
            const token = await getTokens(reply, id);

            reply.send(mapUserWithToken({ user, token }));
        })
        .catch((error) => {
            console.log(error);
            reply.send({
                error: {
                    msg: 'You are alredy register',
                    error
                }
            })

        })


}

const options = body({
    name: Joi.string().required(),
    email: Joi.string().email({ minDomainSegments: 2 }).required(),
    password: Joi.string().min(3).max(15).required(),
});

export default {
    handler,
    options
}

