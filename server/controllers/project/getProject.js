import { getProjectObject, mapProject } from "../../mappers/project";
import Project from "../../models/project";
import { GetObject } from "../../sheard/role";

async function handler(req, reply) {

    const { projectId } = req.params
    const { id } = req.user

    Project.findById(projectId).populate({
        path: 'columns',
        populate: {
            path: 'tasks',
            model: 'task'
        }
    })
        .then((_project) => {
            console.log(_project);

            if (_project) {
                const project = mapProject(_project)
                console.log(GetObject(project).checkAccess(id));
                if (GetObject(project).checkAccess(id)) reply.send(project);
            }


            throw Error('403')
        }).catch((error) => {
            console.log(error);
            reply.status(403).send(error)
        })

}

const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

