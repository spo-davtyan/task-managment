import { mapProject, mapProjects } from "../../mappers/project";
import Project from "../../models/project";

async function handler(req, reply) {

    const { ids } = req.query
    const projectsIds = ids.split(',').map((i) => i.trim())
    Project.find().where('_id').in(projectsIds).select("_id, name")
        .then((_projects) => {
            const projects = mapProjects(_projects)


            console.log('asdasd', projects);
            return reply.send(projects);

        }).catch((error) => reply.status(403).send(error))

}

const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

