import { ROLE } from "../../configs/constant";
import { mapProject } from "../../mappers/project";
import Project from "../../models/project";
import User from "../../models/user";

async function handler(req, reply) {
    const { name } = req.body
    const { id } = req.user

    Project.create({
        name,
        users: {
            [id]: ROLE.OWNER
        },
        columns: []
    }).then((project) => {
        User.findById(id).then(async (user) => {
            user.projects = [
                ...user.projects,
                project._id
            ]
            await user.save()
            reply.send(mapProject(project))
        }).catch((error) => reply.status(400).send({ error: error.toString() }))
    }).catch((error) => reply.status(400).send({ error: error.toString() }))

}

const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

