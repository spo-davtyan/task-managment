import Joi from "joi";
import { body } from "../../sheard/schema";
import User from "../../models/user";
import { mapUser } from "../../mappers/user";
async function handler(req, reply) {
    const { id } = req.user
    const { name } = req.body

    User.findByIdAndUpdate(id, {
        name
    }, { new: true })
        .then(async (user) => {
            reply.send(mapUser(user));
        })
        .catch((error) => {

            reply.send({
                error: {
                    msg: 'Its funny , but you cant change name',
                    error
                }
            })

        })
}

const options = (preValidation) => {
    return {
        ...body({
            name: Joi.string().required(),
        }),
        preValidation
    }
};

export default {
    handler,
    options
}

