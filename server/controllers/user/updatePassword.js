import Joi from "joi";
import { body } from "../../sheard/schema";
import User from "../../models/user";
import { mapUser } from "../../mappers/user";
async function handler(req, reply) {
    const { id } = req.user

    // TODO: check current password
    const password = await req.bcryptHash(req.body.password)
    User.findByIdAndUpdate(id, {
        password
    }, { new: true })
        .then(async (user) => {
            reply.send(mapUser(user));
        })
        .catch((error) => {

            reply.send({
                error: {
                    msg: 'Its funny , but you cant change Your password',
                    error
                }
            })

        })
}

const options = (preValidation) => {
    return {
        ...body({
            current: Joi.string().min(3).max(15).required(),
            password: Joi.string().min(3).max(15).required(),
            confirm: Joi.any().valid(Joi.ref('password')).required()
        }),
        preValidation
    }
};

export default {
    handler,
    options
}

