import Column from "../../models/column";
import Project from "../../models/project";
import { removeElement } from "../../sheard/arrayRemoveElement";
import Task from "../../models/task";
import mongoose from './../../configs/mongo'
async function handler(req, reply) {
    const { tasksIdList } = req.body;
    const { columnsId } = req.params;

    try {

        await Task.findByIdAndDelete(taskId);
        let column = await Column.findById(columnId);
        column.tasks = removeElement(column.tasks, taskId)
        await column.save()
        reply.status(200).send();
    }
    catch (e) {

        reply.status(400).send(e)
    }
    finally {

    }


}
const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

