import Column from "../../models/column";
import { removeElement } from "../../sheard/arrayRemoveElement";
import Task from "../../models/task";
async function handler(req, reply) {
    const { columnId, taskId } = req.params;

    try {
        await Task.findByIdAndDelete(taskId);
        let column = await Column.findById(columnId);
        column.tasks = removeElement(column.tasks, taskId)
        await column.save()
        reply.status(200).send();
    }
    catch (e) {
        reply.status(400).send(e)
    }

}
const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

