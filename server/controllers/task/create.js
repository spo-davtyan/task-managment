import Column from "../../models/column";
import Task from "../../models/task";
import { mapTask } from "../../mappers/project";
async function handler(req, reply) {
    const { name, columnId, order } = req.body
    console.log(name, columnId, order);
    Task.create({
        name,
        members: [],
        order,
    }).then((task) => {
        Column.findById(columnId).then(async (column) => {
            column.tasks = [
                ...column.tasks,
                task._id
            ]
            await column.save()
            reply.send(mapTask(task))
        }).catch((error) => reply.status(400).send({ error: error.toString() }))
    }).catch((error) => reply.status(400).send({ error: error.toString() }))

}

const options = (preValidation) => {
    return {
        preValidation
    }
};
export default {
    handler,
    options
}

