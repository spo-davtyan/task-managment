export const mapUserWithToken = ({ user, token }) => {
    const { access } = token
    return {
        ...mapUser(user),
        // refresh,
        access
    }
}
export const mapUser = (user) => {
    const { name, email, _id, projects } = user
    return {
        id: _id,
        name,
        email,
        projects
    }
}