import { ROLE } from "../configs/constant"
export const mapProjects = (projects) => {
    return projects.map(p => mapProject(p))

}
export const mapProject = (p) => {
    const { name, users, columns, _id } = p
    return {
        id: _id,
        name,
        users,
        columns: columns ? columns.map(c => mapColumn(c)) : []
    }
}

export const mapColumn = (c) => {
    const { name, order, tasks, _id } = c
    return {
        id: _id,
        name,
        order,
        tasks: tasks ? tasks.map(t => mapTask(t)) : []
    }
}
export const mapTask = (t) => {
    const { name, order, member, _id } = t
    return {
        id: _id,
        name, order, member
    }
}
export const getProjectObject = (project) => {
    return {
        ...mapProject(project),
        hasAccess: function (userId) {
            return this.users[userId]
        },
        isOwner: function (userId) {
            return this.users[userId] && this.users[userId] === ROLE.OWNER
        },
        isUser: function (userId) {
            return this.users[userId] === ROLE.USER
        },
    }
} 