import { authenticate } from './authenticate'
export const decorators = (app) => {
    authenticate(app)
}