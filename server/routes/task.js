import create from "../controllers/task/create"
import deleteTask from "../controllers/task/delete"
export const task = (app) => {
    app.post('/tasks/create', create.options([app.authenticate]), create.handler)
    app.delete('/columns/:columnId/tasks/:taskId', deleteTask.options([app.authenticate]), deleteTask.handler)

    // app.get('/projects/:projectId', getProject.options([app.authenticate]), getProject.handler)
    // app.get('/projects', getProjects.options([app.authenticate]), getProjects.handler)
}
