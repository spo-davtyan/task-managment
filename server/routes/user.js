import updateName from "../controllers/user/updateName"
import updatePassword from "../controllers/user/updatePassword"

export const user = (app) => {
    app.post('/user/update/name', updateName.options([app.authenticate]), updateName.handler)
    app.post('/user/update/password', updatePassword.options([app.authenticate]), updatePassword.handler)
}
