import createroject from "../controllers/project/create"
import getProject from "../controllers/project/getProject"
import getProjects from "../controllers/project/getProjects"

export const project = (app) => {
    app.post('/projects/create', createroject.options([app.authenticate]), createroject.handler)
    app.get('/projects/:projectId', getProject.options([app.authenticate]), getProject.handler)
    app.get('/projects', getProjects.options([app.authenticate]), getProjects.handler)
}
