import { auth } from './auth'
import { user } from './user'
import { project } from './project'
import { column } from './column'
import { task } from './task'

export const registerRoutes = (app) => {
    auth(app)
    user(app)
    project(app)
    column(app)
    task(app)

}