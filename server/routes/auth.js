import login from "../controllers/auth/login"
import register from "../controllers/auth/register"
import verify from "../controllers/auth/verify"
import refresh from "../controllers/auth/refresh"

export const auth = (app) => {
    app.post('/register', register.options, register.handler)
    app.post('/login', login.options, login.handler)
    app.post('/refresh', refresh.options, (req, res) => refresh.handler(req, res, app))

    app.post('/verify', verify.options(app.authenticate), verify.handler)
}
