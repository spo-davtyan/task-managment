import createcolumn from "../controllers/column/create"
import deletecolumn from "../controllers/column/delete"

export const column = (app) => {
    app.post('/columns/create', createcolumn.options([app.authenticate]), createcolumn.handler)
    app.delete('/projects/:projectId/columns/:columnId', deletecolumn.options([app.authenticate]), deletecolumn.handler)
    // app.get('/projects/:projectId', getProject.options([app.authenticate]), getProject.handler)
    // app.get('/projects', getProjects.options([app.authenticate]), getProjects.handler)
}
